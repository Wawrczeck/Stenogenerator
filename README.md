# Stenogenerator

Stenogenerator, der normalen Text (Langschrift) in ein Stenogramm (Kurzschrift) überträgt.
Aktuelles Kurzschriftsystem: Deutsche Einheitskurzschrift (DEK)

![Programm-Maske zur Textübertragung](Screenshots/TextUebertragen.webp)
